# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

import os
import csv

class CrawlerPipeline(object):
    def __init__(self):
        store_file = os.path.dirname(__file__)+'vnexpress.csv'        
        self.myCsv = csv.writer(open(store_file, 'wb', encoding='utf-8'))

    def process_item(self, item, spider):
        return item        
