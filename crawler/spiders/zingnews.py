import scrapy
#from unidecode import unidecode

class CrawlerSpider(scrapy.Spider):
    name = 'zingnews'
    allowed_domains = ['zingnews.vn']
    start_urls = [
        'https://zingnews.vn/ly-do-ngo-manh-dat-tu-choi-hop-tac-voi-chau-tinh-tri-post1135582.html'
    ]

    def parse(self, response):
        titleDetail = response.xpath('//h1[@class="the-article-title"]/text()').get()
        dateDetail  = response.xpath('//li[@class="the-article-publish"]/text()').get()
        authorDetail  = response.xpath('//div[@class="the-article-credit"]/p[@class="author"]/text()').get()

        yield {
            "Title": titleDetail,
            'Date': dateDetail,
            'Author': authorDetail,
            'Url': response.request.url
        }

        next_page = response.css('article.article-item a::attr(href)').get()      
        if next_page is not None:
            yield response.follow(next_page, callback=self.parse)