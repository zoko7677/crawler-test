from scrapy.exporters import CsvItemExporter

class CSVkwItemExporter(CsvItemExporter):

       def __init__(self, *args, **kwargs):           
           kwargs['encoding'] = 'utf-8'
           super(CSVkwItemExporter, self).__init__(*args, **kwargs)