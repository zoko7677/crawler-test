Crawler nội dung tin trang zing news bằng ngôn ngữ Python

<b>Cài đặt thư viện cần thiết</b>
1. Download và cài đặt ngôn ngữ python tại https://www.python.org/downloads/ phiên bản 3.8.5
2. Cài đặt thư viện Scrapy: <b>pip install scrapy</b>
3. Cài đặt thư viện scrapy-xlsx: <b>pip install scrapy-xlsx</b>

<b>Chạy crawler</b><br>
1. Mở giao diện chạy lệnh Command-Line (trên Window), di chuyển vào thư mục chứa code mới clone về
2. Di chuyển vào thư mục crawler-test\crawler\spiders
3. Chạy lệnh:  <b>py -m scrapy crawl zingnews</b>

Note: File Excel Export sẽ được lưu trữ trong thư mục crawler-test\crawler\spiders
   